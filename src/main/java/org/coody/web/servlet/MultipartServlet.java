package org.coody.web.servlet;

import java.io.IOException;

import org.coody.framework.minicat.annotation.Servlet;
import org.coody.framework.minicat.entity.HttpServletRequest;
import org.coody.framework.minicat.entity.HttpServletResponse;
import org.coody.framework.minicat.entity.MultipartFile;
import org.coody.framework.minicat.servlet.HttpServlet;
import org.coody.web.util.FileUtils;

@Servlet("/upload.do")
public class MultipartServlet extends HttpServlet{

	@Override
	public void doService(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id=request.getParament("id");
		System.out.println(id);
		System.out.println(request.getParament("name"));
		System.out.println(request.getParament("type"));
		System.out.println(request.getParament("lastModifiedDate"));
		System.out.println(request.getParament("size"));
		MultipartFile file=request.getFile("upfile");
		System.out.println(file.getFileName());
		FileUtils.writeFile("d://coody.png", file.getFileContext());
	}

}
